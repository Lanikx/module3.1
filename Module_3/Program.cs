﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Module_3
{
    static class Program
    {
        static void Main(string[] args)
        {
            string input = Console.ReadLine();
            string[] digits = input.Split();
            bool flag;
            int result;
            Task1 task1 = new Task1();
            Task2 task2 = new Task2();
            Task3 task3 = new Task3();

            result = task1.Multiplication(task1.ParseAndValidateIntegerNumber(digits[0]), task1.ParseAndValidateIntegerNumber(digits[1]));
            Console.WriteLine(result);

            while (true)
            {
                Console.WriteLine("Введите натуральное число");
                input = Console.ReadLine();
                flag = task2.TryParseNaturalNumber(input, out result);
                if (flag)
                {
                    break;
                }
            }

            int digit;
            task3.TryParseNaturalNumber(Console.ReadLine(), out result);
            task3.TryParseNaturalNumber(Console.ReadLine(), out digit);

            Console.WriteLine(task3.RemoveDigitFromNumber(result, digit));
        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            int result;
            bool flag;

            flag = int.TryParse(source, out result);
            if (flag)
            {
                return result;
            }
            else
            {
                throw new ArgumentException(" ");
            }
        }

        public int Multiplication(int num1, int num2)
        {
            int result = 0;
            for (int i = 0; i < Math.Abs(num2); i++)
            {
                result += num1;
            }
            if (num2 < 0)
            {
                result *= (-1);
            }
            return result;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            bool flag;
            flag = int.TryParse(input, out result);
            if (result >= 0 && flag)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            List<int> list1 = new List<int>();
            for (int i = 0; i < naturalNumber; i++)
            {
                if (i % 2 == 0)
                    list1.Add(i);
            }
            return list1;
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            bool flag;
            flag = int.TryParse(input,out result);
            
            if (result >= 0 && flag)
                return true;
            return false;
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            StringBuilder sys = new StringBuilder();                    
            char[] digit = digitToRemove.ToString().ToCharArray();
            char[] number = source.ToString().ToCharArray();
            for (int i = 0; i < number.Length; i++)
            {
                if (number[i] != digit[0])
                    sys.Append(number[i]);
            }
            return sys.ToString();
        }
    }
}
